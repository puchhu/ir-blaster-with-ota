#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#ifndef UNIT_TEST

#include <Arduino.h>
#endif
#include <IRremoteESP8266.h>
#include <IRsend.h>
#define BLYNK_PRINT Serial
//#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <ir_Toshiba.h>

//char auth[] ="fX7gbEQyNG-3PVjsbTRt-hbRG7SfWwVn";                /////////////Raj/////////////////////
char ssid[] ="Raj";
char pass[] ="raj@00321";
const uint16_t kIrLed = 5;  // ESP8266 GPIO pin to use. Recommended: 5 (D1).
const uint16_t kIrLed2 = 4;  // ESP8266 GPIO pin to use. Recommended: 4 (D2).

IRsend irsend(kIrLed);  // Set the GPIO to be used to sending the message.
IRsend irsendAC(kIrLed2);
uint16_t powerOffAC[295] = {4390, 4372,  546, 1616,  550, 1620,  544, 1572,  594, 1620,  546, 506,  576, 534,  548, 1618,  548, 536,  546, 506,  576, 534,  548, 536,  548, 534,  548, 1618,  548, 1620,  546, 534,  548, 1618,  546, 536,  546, 536,  546, 534,  548, 534,  548, 536,  546, 536,  546, 1618,  548, 1620,  546, 1592,  574, 1618,  548, 1618,  546, 1618,  546, 1620,  546, 1618,  548, 534,  548, 536,  548, 534,  548, 536,  546, 536,  546, 536,  546, 534,  548, 536,  548, 536,  546, 1588,  578, 534,  548, 534,  548, 534,  548, 536,  546, 534,  550, 536,  546, 534,  548, 538,  544, 1618,  548, 536,  546, 538,  546, 536,  546, 538,  546, 1590,  576, 1618,  548, 1592,  574, 502,  580, 536,  546, 536,  546, 536,  548, 536,  546, 534,  548, 536,  546, 536,  546, 1618,  548, 536,  546, 536,  546, 536,  548, 506,  576, 1618,  546, 1620,  546, 536,  546, 7482,  4390, 4346,  572, 1620,  544, 1584,  582, 1620,  546, 1620,  546, 536,  546, 536,  546, 1590,  576, 536,  548, 536,  546, 504,  578, 536,  546, 534,  548, 1620,  546, 1586,  578, 534,  548, 1620,  546, 534,  548, 534,  548, 536,  546, 536,  546, 510,  572, 536,  546, 1620,  544, 1622,  546, 1620,  544, 1618,  548, 1594,  598, 1594,  546, 1620,  546, 1618,  548, 536,  546, 536,  546, 534,  548, 506,  576, 534,  548, 536,  546, 536,  548, 536,  546, 536,  546, 1618,  548, 536,  546, 536,  548, 536,  546, 536,  548, 536,  546, 534,  548, 536,  546, 534,  548, 1618,  548, 534,  546, 536,  546, 534,  548, 534,  548, 1618,  548, 1620,  546, 1590,  576, 534,  548, 534,  548, 536,  546, 506,  578, 534,  548, 506,  574, 536,  546, 534,  548, 1620,  546, 534,  548, 508,  574, 536,  546, 506,  578, 1620,  546, 1618,  548, 502,  580};  // UNKNOWN 420D0AA7
uint16_t powerOnAC[295] = {4416, 4372,  544, 1620,  546, 1620,  546, 1620,  546, 1618,  548, 504,  578, 538,  544, 1622,  544, 536,  546, 538,  544, 538,  544, 508,  576, 538,  544, 1622,  544, 1618,  548, 538,  544, 1620,  546, 538,  544, 536,  546, 536,  546, 508,  574, 536,  546, 536,  546, 1620,  546, 1620,  546, 1620,  546, 1618,  548, 1620,  544, 1622,  544, 1590,  576, 1620,  546, 536,  546, 536,  546, 536,  546, 510,  572, 538,  544, 538,  546, 538,  544, 538,  544, 538,  544, 1622,  544, 536,  548, 536,  546, 538,  546, 538,  544, 504,  578, 536,  546, 536,  546, 538,  544, 1618,  548, 536,  546, 536,  546, 536,  544, 536,  548, 538,  544, 484,  598, 1620,  546, 536,  546, 504,  578, 538,  544, 536,  546, 536,  546, 504,  578, 510,  572, 534,  548, 1620,  546, 506,  578, 534,  548, 536,  546, 536,  546, 536,  546, 538,  546, 536,  546, 7454,  4418, 4370,  546, 1620,  584, 1556,  572, 1618,  548, 1618,  586, 498,  546, 538,  584, 1552,  612, 500,  584, 500,  582, 500,  584, 498,  584, 498,  580, 1584,  586, 1582,  584, 498,  584, 1582,  582, 466,  616, 496,  586, 500,  582, 498,  614, 468,  614, 468,  614, 1550,  616, 1552,  612, 1550,  614, 1554,  610, 1554,  610, 1558,  576, 1588,  574, 1590,  576, 508,  576, 478,  604, 508,  574, 508,  576, 506,  574, 476,  604, 508,  578, 504,  578, 504,  578, 1590,  576, 476,  606, 474,  610, 506,  574, 506,  578, 506,  574, 508,  574, 480,  602, 508,  574, 1590,  574, 482,  600, 508,  574, 508,  574, 508,  570, 512,  572, 480,  604, 1592,  574, 478,  600, 512,  570, 512,  546, 536,  546, 536,  572, 510,  574, 508,  576, 480,  574, 1592,  574, 534,  548, 536,  546, 536,  546, 536,  548, 536,  546, 538,  546, 534,  548};  // UNKNOWN 180B9ACF
uint16_t powerOnTV[135] = {4474, 4552,  512, 1728,  514, 1730,  512, 1728,  512, 608,  512, 608,  514, 608,  514, 606,  514, 608,  514, 1730,  512, 1730,  514, 1728,  514, 608,  514, 608,  514, 608,  514, 608,  514, 608,  514, 608,  514, 1728,  514, 606,  514, 608,  514, 608,  514, 608,  514, 606,  514, 588,  534, 1728,  542, 580,  514, 1728,  514, 1728,  514, 1728,  512, 1730,  512, 1728,  514, 1728,  512, 46902,  4474, 4550,  514, 1728,  514, 1728,  512, 1728,  514, 608,  514, 608,  514, 608,  512, 608,  512, 608,  512, 1728,  514, 1730,  512, 1728,  514, 606,  514, 608,  514, 608,  514, 608,  514, 608,  512, 608,  514, 1728,  512, 608,  514, 608,  512, 608,  514, 608,  514, 608,  514, 608,  514, 1728,  514, 608,  512, 1730,  560, 1682,  514, 1730,  562, 1680,  512, 1730,  512, 1730,  564};  // SAMSUNG E0E040BF
uint16_t powerOffTV[67] = {4476, 4552,  512, 1706,  536, 1728,  514, 1728,  512, 608,  514, 608,  512, 608,  514, 608,  514, 608,  512, 1730,  514, 1726,  514, 1728,  514, 608,  514, 608,  512, 608,  512, 608,  514, 608,  512, 610,  512, 1728,  514, 606,  514, 608,  512, 608,  514, 608,  512, 608,  514, 608,  512, 1728,  514, 608,  514, 1728,  514, 1728,  514, 1730,  512, 1728,  514, 1728,  514, 1728,  512};  // SAMSUNG E0E040BF

BLYNK_WRITE(V1) {
  int pinValue = param.asInt(); // assigning incoming value from pin V1 to a variable
  if(pinValue == 1){
    irsend.sendRaw(powerOnAC, 295, 38);
    Serial.println("TOSHIBA AC ON");
//    Blynk.virtualWrite(0,"  OFF");
    Blynk.setProperty(V1,"offLabel","ON");
  }
  else{
    irsend.sendRaw(powerOffAC, 295, 38);
    Serial.println("TOSHIBA AC Off");
//    Blynk.virtualWrite(1,"  ON");
    Blynk.setProperty(V1,"offLabel","OFF");

  }
}

BLYNK_WRITE(V2) {
  int pinValue = param.asInt(); // assigning incoming value from pin V1 to a variable
  if(pinValue == 1){
    irsend.sendRaw(powerOnTV, 135, 38);
    Serial.println("TV ON");
//    Blynk.virtualWrite(0,"  OFF");
    Blynk.setProperty(V2,"offLabel","ON");

  }
  else{
    irsend.sendRaw(powerOffTV, 67, 38);
    Serial.println("TV OFF");
//    Blynk.virtualWrite(1,"  ON");
    Blynk.setProperty(V2,"offLabel","OFF");

  }
}



void setup() {
  irsend.begin();
  irsendAC.begin();  
  Serial.begin(115200, SERIAL_8N1, SERIAL_TX_ONLY);
   WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
   
    delay(5000);
    ESP.restart();
  }
  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname("IR_BLASTER");
   ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  //Blynk.begin(auth, ssid, pass);
   Blynk.config("vCRrmY4xKcih3q8_EGja-UVZJ10sD6hc");
   
}
void loop() {
   ArduinoOTA.handle();
  Blynk.run();
}
